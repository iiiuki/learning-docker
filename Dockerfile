FROM node:13-alpine

WORKDIR /app

COPY . .

RUN npm install

# CMD ["npm", "run", "dev"]
# CMD ["npm", "start"]

# Production

RUN npm install -g pm2

CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]